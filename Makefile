CC=fteqcc64
CFLAGS=-Wall -O3



# all: effectinfo.txt progs.dat csprogs.dat
# all: progs.dat csprogs.dat
.PHONY: all
all: progs.dat \
     csprogs.dat \
     progs/blip_ammo.sp2 \
     progs/blip_health.sp2 \
     progs/blip_sentry.sp2 \
     progs/blip_monster.sp2 \
     progs/blip_spawner.sp2 \
     progs/blip_datacore.sp2 \
     effectinfo.txt



# menuqc is only used when you want to replace the main game menus
menu.dat: $(shell find qc -name *.qc) ./qc/menu.src
	$(CC) $(CFLAGS) -srcfile qc/menu.src


progs.dat: $(shell find qc -name *.qc) ./qc/progs.src
	$(CC) $(CFLAGS) -srcfile qc/progs.src


csprogs.dat: $(shell find qc -name *.qc) ./qc/csprogs.src
	$(CC) $(CFLAGS) -srcfile qc/csprogs.src


effectinfo.txt: ./effectinfo.d/*
	./effectinfo.sh > effectinfo.txt

# Radar blip images
# Should be 48x48 png images with the make content at the bottom

progs/blip_ammo.sp2: progs/chat_bubble_yellow.png
	makesp2 $@ $? 1 10 10

progs/blip_health.sp2: progs/chat_bubble_heart.png
	makesp2 $@ $? 1 8 8

progs/blip_sentry.sp2: progs/chat_bubble_smiley_green.png
	makesp2 $@ $? 1 10 10

progs/blip_monster.sp2: progs/skull.png
	makesp2 $@ $? 1 8 8

progs/blip_spawner.sp2: progs/purple_exclamation.png
	makesp2 $@ $? 1 10 10

progs/blip_datacore.sp2: progs/chat_bubble_blue.png
	makesp2 $@ $? 1 10 10
