*water0
{
    dpNoShadow
    deformVertexes move 0 0 1 sin 0 0.25 0 0.25
    {
        map textures/water0.tga
        // tcMod turb 0 0.01 0 0.5    // gentle wobble
        tcMod scroll 0.06 0.06
    }
    {
        map $lightmap
        rgbGen identity
    }
    // <reflectmin> <reflectmax> <refractdistort> <reflectdistort> <refractr> <refractg> <refractb> <reflectr> <reflectg> <reflectb> <alpha>
    dp_water 0.1 1   3 3   1 1 0.65   1 1 1   0.5
}



*water1
{
    program defaultwarp
}



*water12
{
    program foggy
}
