techlzr_red1
{
    cull back
    // deformVertexes wave 256 sin 0 1 0.1 0.25
    // {
    //     map $diffuse
    //     // blendfunc GL_DST_COLOR
    //     // blendfunc GL_DST_COLOR GL_ONE_MINUS_DST_COLOR
    //     blendfunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
    //     // tcMod scale 1.0 1.0
    //     // tcMod turb 1 0.1 0.5 .04
    //     // tcMod scroll 0.0005 0.0005
    //     // tcMod stretch <func> <base> <amplitude><phase> <frequency>
    //     tcMod stretch sin 1 1 4
    // }
    // {
    //     map $diffuse
    //     // blendfunc GL_DST_COLOR GL_ONE_MINUS_DST_COLOR
    //     blendfunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
    //     tcMod scale 1.0 1.0
    //     tcMod turb 0.5 0.075 0.5 0.02
    //     // tcMod scroll 0.005 0.005
    //     tcMod scroll 0.5 0.5
    // }
    // {
    //     map $lightmap
    //     blendfunc GL_DST_COLOR GL_ZERO
    //     rgbgen identity
    // }
    // program defaultwarp
    program techlzr_red1
}
